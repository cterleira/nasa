import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Planet } from "../models/planet.model";

import { throwError } from "rxjs";
import { catchError, take } from "rxjs/operators";
import { DatePipe } from "@angular/common";

import { environment } from "../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class PlanetService {
  private baseUrlApod = environment.BASE_URL_APOD;
  private apiKey = environment.NASA_KEY;

  constructor(private http: HttpClient, public datepipe: DatePipe) {}

  public getPlanets(): Observable<Planet[]> {
    let date = new Date();
    let start_date = this.datepipe.transform(date, "yyyy-MM-dd");
    start_date = "2021-09-07"; // MOCK CAMBIAR
    let end_date = this.datepipe.transform(
      date.setDate(date.getDate() - 6),
      "yyyy-MM-dd"
    );

    const apodUrl =
      this.baseUrlApod +
      "start_date=" +
      end_date +
      "&end_date=" +
      start_date +
      "&api_key=" +
      this.apiKey;

    return this.http.get<Planet[]>(apodUrl).pipe(
      take(1),
      catchError((err: any) => {
        return throwError("Error: ", err);
      })
    );
  }

  public getPlanetDate(date: string): Observable<Planet> {
    const datePlanet = date;
    const apodUrl =
      this.baseUrlApod + "date=" + datePlanet + "&api_key=" + this.apiKey;
    console.log("apodUrl", apodUrl);
    return this.http.get<Planet>(apodUrl).pipe(
      take(1),
      catchError((err: any) => {
        return throwError("Error: ", err);
      })
    );
  }
}
