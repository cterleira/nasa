import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Planet } from "../models/planet.model";
import { PlanetService } from "../services/planet.service";

@Component({
  selector: "app-planet-list",
  templateUrl: "./planet-list.component.html",
  styleUrls: ["./planet-list.component.sass"],
})
export class PlanetListComponent implements OnInit {
  public listPlanet: Planet[];

  constructor(private planetService: PlanetService, public router: Router) {}

  ngOnInit() {
    this.getPlanets();
  }

  getPlanets() {
    this.planetService.getPlanets().subscribe((response: Planet[]) => {
      this.listPlanet = response;
    });
  }

}
