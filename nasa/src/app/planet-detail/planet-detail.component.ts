import { createOfflineCompileUrlResolver } from "@angular/compiler";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Planet } from "../models/planet.model";
import { PlanetService } from "../services/planet.service";

@Component({
  selector: "app-planet-detail",
  templateUrl: "./planet-detail.component.html",
  styleUrls: ["./planet-detail.component.sass"],
})
export class PlanetDetailComponent implements OnInit {
  public listPlanet: Planet[];
  public detailPlanet: Planet;

  constructor(
    private planetService: PlanetService,
    public router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.getPlanetDate();
  }

  getPlanetDate(): void {
    const id = this.route.snapshot.paramMap.get("id");
    this.planetService.getPlanetDate(id).subscribe((response: Planet) => {
      this.detailPlanet = response;
    });
  }
}
